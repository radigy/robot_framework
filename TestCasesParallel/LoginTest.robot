*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/login_keywords.robot

*** Variables ***
${Browser}  headlessfirefox
${SiteUrl}  http://newtours.demoaut.com
${user}  tutorial
${pwd}  tutorial

*** Test Cases ***
LoginTest
    Open my browser  ${SiteUrl}  ${Browser}
    Enter UserName  ${user}
    Enter Password  ${pwd}
    Click SignIn
    Sleep  10
    Verify Succesfull Login
    close my browsers
