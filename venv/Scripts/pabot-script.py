#!C:\Users\ramd\PycharmProjects\Automation\venv\Scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'robotframework-pabot==0.89','console_scripts','pabot'
__requires__ = 'robotframework-pabot==0.89'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('robotframework-pabot==0.89', 'console_scripts', 'pabot')()
    )
