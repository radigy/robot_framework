*** Settings ***
Library  SeleniumLibrary

*** Test Cases ***
HandlingAlerts
    open browser  http://testautomationpractice.blogspot.com/  chrome
    maximize browser window
    click element  xpath://*[@id="HTML9"]/div[1]/button
    sleep  2
    alert should be present  Press a button!
    log to console  alert text as expected
#    handle alert  LEAVE
    sleep  2
    close browser