*** Settings ***
Library  SeleniumLibrary

*** Test Cases ***
MouseActions
    # Right click action / open context menu
    open browser  http://swisnl.github.io/jQuery-contextMenu/demo  chrome
    maximize browser window
    open context menu  xpath://span[@class='context-menu-one btn btn-neutral']
    sleep  3

    #Double click action
    go to  http://testautomationpractice.blogspot.com/
    maximize browser window
    double click element  xpath://button[contains(text(),'Copy Text')]
    sleep  3

    #Drag and drop
    go to  http://www.dhtmlgoodies.com/scripts/drag-drop-custom/demo-drag-drop-3.html
    drag and drop  id:box6  id:box106
    sleep  3

    close browser

