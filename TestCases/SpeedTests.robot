*** Settings ***
Library  SeleniumLibrary

*** Test Cases ***
RegTests
    ${speed}=   get selenium speed
    log to console  ${speed}
    open browser  http://demowebshop.tricentis.com/register  chrome
    maximize browser window
    set selenium speed  1

    select radio button  Gender  M
    input text  id:FirstName  David
    input text  id:LastName  John
    input text  name:Email  anhc@gmail.com
    input text  id:Password  davidjohn
    input text  name:ConfirmPassword  davidjohn

    ${speed}=   get selenium speed
    log to console  ${speed}
#    close browser
