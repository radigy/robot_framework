*** Settings ***
Library  SeleniumLibrary

*** Test Cases ***
ScrollingTest
    open browser  https://www.countries-ofthe-world.com/flags-of-the-world.html  chrome
    maximize browser window
    sleep  1
#    execute javascript  window.scrollTo(0,2500)
#    scroll element into view  xpath://td[contains(text(),'India')]

    execute javascript  window.scrollTo(0,document.body.scrollHeight)   #end of the page
    sleep  1

    execute javascript  window.scrollTo(0,-document.body.scrollHeight)   #top of the page

    sleep  1
    close browser