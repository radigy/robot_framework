*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${browser}  chrome
${url}  http://www.practiceselenium.com/practice-form.html

*** Test Cases ***
Testing Radio Buttons and Checkboxes
    open browser  ${url}  ${browser}
    maximize browser window
    set selenium speed  0.25 seconds

    select radio button  sex  Female
    select radio button  exp  5

    select checkbox  id:tea1
    select checkbox  name:RedTea

    unselect checkbox  id:tea1
    unselect checkbox  name:RedTea

    close browser
