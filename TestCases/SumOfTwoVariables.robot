*** Variables ***



*** Test Cases ***
sumOfVariables
    ${prod1}=  set variable  ${42.42}
    ${prod2}=  set variable  ${20.22}
    ${totalPrice}=  Evaluate  ${prod1}+${prod2}
    log to console  ${prod1}
    log to console  ${prod2}
    log to console  ${totalPrice}
