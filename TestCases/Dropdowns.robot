*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary

*** Variables ***
${browser}  chrome
${url}  http://www.practiceselenium.com/practice-form.html

*** Test Cases ***
Handling Dropdowns and List boxex
    open browser  ${url}  ${browser}
    maximize browser window

    sleep  2
    select from list by label  id:continents  Australia
    sleep  2
    select from list by index  id:continents  6
    sleep  2
    select from list by index  id:selenium_commands  1  2  3  4
    sleep  2
    unselect from list by index  id:selenium_commands  2
    sleep  2

    close browser


*** Keywords ***
Provided precondition
    Setup system under test