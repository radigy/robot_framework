*** Settings ***
Library  SeleniumLibrary
Library  SeleniumLibrary

*** Test Cases ***
RegTests

    open browser  http://demowebshop.tricentis.com/register  chrome
    maximize browser window
    ${time}=  get selenium timeout
    log to console  ${time}

    set selenium timeout  10 seconds
    wait until page contains  Registration
    select radio button  Gender  M
    input text  id:FirstName  David
    input text  id:LastName  John
    input text  name:Email  anhc@gmail.com
    input text  id:Password  davidjohn
    input text  name:ConfirmPassword  davidjohn
    close browser


