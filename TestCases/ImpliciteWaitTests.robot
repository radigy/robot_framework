*** Settings ***
Library  SeleniumLibrary
Library  SeleniumLibrary

*** Test Cases ***
RegTests

    open browser  http://demowebshop.tricentis.com/register  chrome
    maximize browser window
    ${implicitTime}=  get selenium implicit wait
    log to console  ${implicitTime}

    set selenium implicit wait  10 seconds
    ${implicitTime}=  get selenium implicit wait
    log to console  ${implicitTime}

    select radio button  Gender  M
    input text  id:FirstName  David
    input text  id:LastName  John
    input text  name:Email  anhc@gmail.com
    input text  id:Password  davidjohn
    input text  name:ConfirmPassword  davidjohn
    close browser


