*** Test Cases ***
#ForLoop1
#    : FOR  ${i}  IN RANGE  1  10
#    \  Log to console  ${i}

#ForLoop2
#    : FOR  ${i}  IN  1  2  3  4  5  6  7  8
#    \  log to console  ${i}

#ForLoop3
#    @{items}  create list  1  2  3  4  5
#    : FOR  ${i}  IN  @{items}
#    \  log to console  ${i}

#ForLoop4
#    : FOR  ${i}  IN  john  david  smith  scott
#    \  log to console  ${i}

#ForLoop5
#    @{nameslist}  create list  john  david  smith  scott
#    : FOR  ${i}  IN  @{nameslist}
#    \  log to console  ${i}

ForLoop6withExit
    @{items}  create list  1  2  3  4  5
    : FOR  ${i}  IN  @{items}
    \  log to console  ${i}
    \  exit for loop if  ${i}==3



