*** Settings ***
Library  SeleniumLibrary

*** Test Cases ***
TestingFrames
    open browser  https://seleniumhq.github.io/selenium/docs/api/java/index  chrome
    maximize browser window

    select frame  packageListFrame
    sleep  2
    click link  org.openqa.selenium
    sleep  2
    unselect frame

    sleep  2
    select frame  packageFrame
    sleep  2
    click link  WebDriver
    unselect frame

    sleep  2

    select frame  classFrame
    click link  Help
    unselect Frame

    sleep  2
    close browser
